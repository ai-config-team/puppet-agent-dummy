Name: puppet-agent
Version: 0.0.0
Release: 5%{?dist}
Summary: Dummy Package to Aid Upgrade
Group:    Development/Languages
License:  Apache 2
URL:      http://gitlab.cern.ch/ai-config-team/puppet-agent-dummy
Requires: rsync
%if 0%{?el5}
BuildRoot: %(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)
%endif


%description
Dummy Package so that first real puppet-agent install
is actually an upgrade and not an install.

A daily cron job is also included to warm up the puppet cache in it's
new location. This will be dropped with the real puppet-agent
package is deployed.


%setup
#Nothing to setup.

%install
mkdir -p %{buildroot}/opt/puppetlabs/bin
mkdir -p %{buildroot}/opt/puppetlabs/puppet/cache
mkdir -p %{buildroot}/etc/cron.daily

cat <<EOF> %{buildroot}/etc/cron.daily/puppet3topuppet4-cache.sh

if [ -r /var/lib/puppet ] && [ -w /opt/puppetlabs/puppet/cache ] ; then
  /usr/bin/rsync -a --xattrs \
                    /var/lib/puppet/lib \\
                    /var/lib/puppet/facts.d \\
                    /var/lib/puppet/concat \\
                    /var/lib/puppet/*.yaml \\
                    /var/lib/puppet/*.txt \\
                    /var/lib/puppet/clientbucket \\
                    /var/lib/puppet/tbag* \\
                    /opt/puppetlabs/puppet/cache/.  >/dev/null 2>&1
  /usr/bin/rsync -a --xattrs /etc/puppet/landb_config.yaml  \\
           /etc/puppetlabs/puppet/landb_config.yaml >/dev/null 2>&1

fi

true

EOF

cat <<EOF > %{buildroot}/opt/puppetlabs/bin/puppet
#!/bin/bash

exec /usr/bin/puppet \$@

EOF

%files
%dir /opt/puppetlabs/
%dir /opt/puppetlabs/bin
%dir /opt/puppetlabs/puppet
%dir /opt/puppetlabs/puppet/cache
%attr(0755,root,root) /opt/puppetlabs/bin/puppet
%attr(0755,root,root) /etc/cron.daily/puppet3topuppet4-cache.sh

%postun

# Shoot the old puppet if it is running.
if [ -x  /usr/bin/pkill ] ; then
  /usr/bin/pkill -f /usr/bin/puppet
  sleep 5
  /usr/bin/pkill -9 -f /usr/bin/puppet
  /usr/bin/pkill -f  /usr/sbin/mcollectived
  sleep 5
  /usr/bin/pkill -9 -f  /usr/sbin/mcollectived

  :
fi
# Restart the new puppet.
%if 0%{?el5}%{?el6} 
service puppet restart
chkconfig --add puppet
chkconfig puppet on
service mcollective restart
chkconfig --add mcollective
chkconfig mcollective on
:
%else
systemctl restart puppet.service 
systemctl enable puppet.service
systemctl restart mcollective.service 
systemctl enable mcollective.service

:
%endif

%changelog
* Fri Mar 17 2017 Steve Traylen <steve.traylen@cern.ch> - 0.0.0-5
- Kill off old mcollective also.

* Fri Mar 10 2017 Steve Traylen <steve.traylen@cern.ch> - 0.0.0-4
- Add *.txt to get cern_os_metadata_cache.txt.

* Fri Mar 10 2017 Steve Traylen <steve.traylen@cern.ch> - 0.0.0-3
- Be more precise about rsync
- Shift over landb cache.

* Thu Mar 9 2017 Steve Traylen <steve.traylen@cern.ch> - 0.0.0-2
- Add BuildRoot to .el5

* Thu Mar 9 2017 Steve Traylen <steve.traylen@cern.ch> - 0.0.0-1
- Add daily cron job to sync up puppet cache.

* Mon Mar 6 2017 Steve Traylen <steve.traylen@cern.ch> - 0.0.0-0
- Dummy package.


